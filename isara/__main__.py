import argparse
import os
import platform
import sys
import re
import shutil
#import warnings
from collections import OrderedDict
from subprocess import Popen, PIPE, check_output  # TODO replace with run in 3.5+
from time import localtime, strftime, sleep, time

from appdirs import user_data_dir
from colorama import Back, Fore, Style
from colorama import init as colorama_init
from yaml import dump
from yaml import load as load_yaml

__version__ = '0.3a1'

system_string = platform.system()

class PathEntry(object):
    """ Object representing path entry in config """
    raw = ''
    prefix = ''
    fullpath = ''
    recursive = False
    valid = False
    isdir = None
    size = 0
    regex_du = re.compile('^(?P<bytes>\d+)')

    def __init__(self, raw_coded_path, prefix):
        self.raw = raw_coded_path
        self.prefix = prefix

    def _decode(self, raw=None):
        recursive = False
        isdir = False
        if raw is None:
            raw = self.raw
        path = raw
        if path[-1] == '*':
            path = path[:-1]
            recursive = True
            isdir = True
        if path[-1] == '/':
            isdir = True
        return path, recursive, isdir

    def validate(self):
        valid = False
        path, recursive, isdir = self._decode() 
        fullpath = os.path.join(self.prefix, path)
        if isdir:
            valid = os.path.isdir(fullpath)
        else:
            valid = os.path.isfile(fullpath)
        if valid:
            if system_string == 'Linux':
                output = check_output(["du", "-s", fullpath]).decode("utf-8")
                match = re.match(self.regex_du, output)
                if match:
                    self.size = int(match.group('bytes'))

            self.fullpath = fullpath
            self.isdir = isdir
            self.valid = valid
            self.recursive = recursive
        return self

    def __str__(self):
        if self.fullpath:
            string = "'{0.fullpath}' ({0.raw})"
        else:
            string = '{0.raw}'
        return string.format(self)

class IsaraCommand(object):
    help = ''
    args = None
    version = __version__
    start_text = '* {1.BLUE}ISARA{2.RESET_ALL} backuper ver.{1.MAGENTA}{0.version}{2.RESET_ALL} kidou! (v={0.args.verbosity})'
    config_path = user_data_dir('IsaraBackup', 'GinFuyou', roaming=True)
    config = None
    size_limit = 0
    home_paths = []
    temp_dir = ''
    rsync_args = ['rsync', '-a', '-m', '-R', '--progress']  # '-d', '-m', '-l', '-pgo', '-t',

    def get_fore(self, colour='WHITE'):
        return getattr(Fore, colour.upper(), 'WHITE')

    def create_parser(self):
        """
        Create and return the ``ArgumentParser`` which will be used to
        parse the arguments to this command.

        """
        parser = argparse.ArgumentParser(description=self.help)
        parser.add_argument('--version', action='version', version=self.version)
        parser.add_argument('-v', '--verbosity', action='count', dest='verbosity', default=0,
            #type=int, # choices=[0, 1, 2, 3],
            help='Verbosity level; 0=minimal output, 1=normal output, 2=verbose output, 3=very verbose output')
        parser.add_argument('--traceback', action='store_true',
            help='Raise on CommandError exceptions')
        parser.add_argument('--no-color', action='store_true', dest='no_color', default=False,
            help="Don't colorize the command output.")
        parser.add_argument('--dryrun', action='store_true', help="Don't copy anything")

        return parser

    def execute(self):
        """
        """
        parser = self.create_parser()
        self.args = parser.parse_args()
        colorama_init()

        start_text = self.start_text.format(self, Fore, Style)
        self.vprint(start_text, v=1)
        # check system and user
        if system_string == 'Linux':
            uid = os.geteuid() # NOTE: not getuid, effective one
            username = os.getlogin()
            self.vprint("* Running on {0} by {1} uid {2} ".format(system_string, username, uid))
            if username == 'root' or uid == 0:
                raise RuntimeError("Not designed to run as root!")
        elif system_string == 'Windows':
            self.vprint("* Running on {0}".format(system_string))
        else:
            self.vprint("! Running on an unknown platform!", colour='RED')

        self.init_config(self.config_path)
        self.handle()

    def vprint(self, text, v=2, end='\n' , colour=None):
        if self.args.verbosity >= v:
            if colour:
                f = self.get_fore(colour)
                text = "{0}{1}{2.RESET_ALL}".format(f, text, Style)
            print(text, end=end)

    def run_rsync(self, args=[]):
        rsync_args = self.rsync_args.copy()
        if self.args.dryrun:
            rsync_args.append('--dry-run')
        if self.args.verbosity >= 1:
            rsync_args.append('-v')
        rsync_args += args
        if self.args.verbosity > 1:
            self.vprint("Rsync args:", v=3)
            for arg in rsync_args:
                self.vprint(arg, v=3)
        process = Popen(rsync_args, stdout=PIPE, stderr=PIPE)
        returncode = None
        while returncode is None:
            sleep(0.1)
            returncode = process.poll()
        return returncode

    def build_recursive_paths(self, input_list, prefix):
        if prefix == '~':
            prefix = os.path.expanduser("~")
        path_list = []
        for rawpath in input_list:
            if isinstance(rawpath, dict):
                for key, sublist in rawpath.items():
                    path_list += self.build_recursive_paths(sublist, os.path.join(prefix, key))
            else:
                self.vprint('{0: >16} :'.format(rawpath), v=2, end=" ")
                path_object = PathEntry(rawpath, prefix).validate()
                if path_object.valid:
                    if path_object.size <= self.size_limit:
                        path_list.append(path_object)
                        if path_object.isdir:
                            colour = 'YELLOW'
                        else:
                            colour = "BLUE"
                        self.vprint("{0} ({1:.2f} KB)".format(path_object, path_object.size/1024), colour=colour)
                    else:
                        self.vprint("{1} skip: size {0:.2f} KB".format(path_object.size/1024, path_object), colour="RED")
                else:
                    self.vprint("INVALID", colour='RED')
        return path_list


    def init_config(self, path):
        """ initialize config dictionary by defaults and conf file """
        config_filepath = os.path.join(path, 'isaraconf.yaml')
        self.vprint('? config path is "{0}"'.format(config_filepath))
        if os.path.isfile(config_filepath):
            self.config = load_yaml(open(config_filepath))
            self.size_limit = self.config.get('size_limit', 64*1024*1024)
            self.vprint("? size limit: {0:.2f} KB".format(self.size_limit/1024))

            if self.config['home_backup']:
                self.home_paths = self.build_recursive_paths(self.config['home_backup'], '~')
            temp_dir = self.config['backup_temp_path']
            if temp_dir[0] != '/':
                temp_dir = os.path.join(os.path.expanduser('~'), temp_dir)
            self.temp_dir = temp_dir
        else:
            os.makedirs(path, exist_ok=True)
            raise RuntimeError("No config file found at '{0}'".format(config_filepath))

    def handle(self):
        path_list = [obj.fullpath for obj in self.home_paths]
        path_list.append(self.temp_dir)
        self.run_rsync(path_list)
"""
msg = "* "
if self.args.dryrun:
    msg += "MOCK "
msg += 'copy:'
self.vprint(msg, v=1, colour='BLUE', end=' ')
if path_object.isdir and path_object.recursive:
    self.vprint(path_object.fullpath, v=1, colour='YELLOW')
    if not self.args.dryrun:
        shutil.copytree(path_object.fullpath, self.temp_dir)
elif path_object.isdir is False:
    self.vprint(path_object.fullpath, v=1)
    if not self.args.dryrun:
        shutil.copy(path_object.fullpath, self.temp_dir)
"""



if __name__ == '__main__':
    IsaraCommand().execute()


"""
DIR="/home/Gin"
TARPATH="$DIR/Gin-bckp.tar"

K4APP=".kde4/share/apps"
K4CNF=".kde4/share/config"

ETCCNF="$DIR/Setup/conf/"

mkdir -p "$ETCCNF"

cp "/etc/init.d/supervisord" "$ETCCNF"
cp "/etc/supervisord.conf" "$ETCCNF"
cp "/etc/redis/6379.conf" "$ETCCNF"
cp "/etc/sysconfig/SuSEfirewall2" "$ETCCNF"
cp -R "/etc/nginx/" "$ETCCNF"


KEY_FILE="$DIR/private.key"
"""
